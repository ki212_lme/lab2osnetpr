:: 1

@echo off
SETLOCAL ENABLEDELAYEDEXPANSION
SETLOCAL ENABLEEXTENSIONS
set log_file=%1
set dir_for_arch=%2
set end_proc=%3
set dir_for_saving_arch=%4
set ip_pc=%5
set size_of_log=%6



:: 2-4

if not exist %log_file% (
echo ------------------------------------------------------------------------------------ > %log_file%
for /F "" %%i in ('time /t') do set mytime=%%i
echo %date% %mytime% >> %log_file%
echo File with name %log_file% created >> %log_file%
echo ------------------------------------------------------------------------------------ >> %log_file%
echo log file created
)



:: 5

for /f "skip=1 delims=" %%A in (
'wmic computersystem get name'
) do for /f "" %%B in ("%%A") do set "compName=%%A"
::echo nameofcomp=%compName%
echo ------------------------------------------------------------------------------------ >> %log_file%
for /F "" %%i in ('time /t') do set mytime=%%i
echo %date% %mytime% >> %log_file%
net start w32time >nul 2>&1
echo Service w32time started 
w32tm /config /computer:%compName% /update >nul 2>&1 
w32tm /resync >nul 2>&1
echo Time synchronization with the NFT server
for /F "" %%i in ('time /t') do set mytime=%%i
echo Time after synchronization with the NFT server:%mytime% >> %log_file%
echo ------------------------------------------------------------------------------------ >> %log_file%



::6

echo Send list of running process in %log_file%
echo ------------------------------------------------------------------------------------ >> %log_file%
for /F "" %%i in ('time /t') do set mytime=%%i
echo %date% %mytime% >> %log_file% 2>&1
echo List of all processes in this system: >> %log_file%
tasklist >> %log_file% 2>&1
echo ------------------------------------------------------------------------------------ >> %log_file%



::7

if not [%3] == [] (
echo Terminating process %end_proc%
echo ------------------------------------------------------------------------------------ >> %log_file%
for /F "" %%i in ('time /t') do set mytime=%%i
echo %date% %mytime% >> %log_file%
echo Result of terminating process %end_proc%: >> %log_file%
taskkill /f /im %end_proc% >> %log_file% 2>&1
echo ------------------------------------------------------------------------------------ >> %log_file%
echo Result of terminating %end_proc% was writen in %log_file%
)



::8-9

if not [%4] == [] (
echo Deleting tmp files
set COUNTER=0
set tempor=Could Not Find
echo Result of deleting files with name on start or with extension tmp was writen in %log_file%
echo ------------------------------------------------------------------------------------ >> %log_file%
for /F "" %%i in ('time /t') do set mytime=%%i
echo %date% %mytime% >> %log_file%
echo Result of deleting tmp files in directory %dir_for_arch% >> %log_file%
for /F "" %%i in ('del /s %dir_for_arch%\*.tmp 2^>nul') do set /A COUNTER=%COUNTER%+1 
for /F "" %%i in ('del /s %dir_for_arch%\tmp* 2^>nul') do set /A COUNTER=%COUNTER%+1
echo Number of deleted files = %COUNTER% >> %log_file%
echo ------------------------------------------------------------------------------------ >> %log_file%
)



::10-11
if not [%4] == [] (
echo Creating archives file fro dir %dir_for_arch%
echo ------------------------------------------------------------------------------------ >> %log_file%
powershell Compress-Archive %dir_for_arch% %dir_for_saving_arch%\%DATE:~-10,2%.%DATE:~-7,2%.%DATE:~-4%_%mytime:~-5,2%.%mytime:~-2,2%.zip 2>&1 
)



::12-13

if not [%4] == [] (
echo Checking archives from yesterday
echo ------------------------------------------------------------------------------------ >> %log_file%
for /F "" %%i in ('time /t') do set mytime=%%i
echo %date% %mytime% >> %log_file%
echo Files from yesterday: >> %log_file%
forfiles /d 1 >> %log_file% 2>&1
echo ------------------------------------------------------------------------------------ >> %log_file%
)



::14

if not [%4] == [] (
echo Deleting archives that have existed more than 30
forfiles /p "%dir_for_arch%" /s /m *.* /D -30 /C "cmd /c del @path" 2>&1
if %errorlevel%==0 (
echo ------------------------------------------------------------------------------------ >> %log_file%
for /F "" %%i in ('time /t') do set mytime=%%i
echo %date% %mytime% >> %log_file%
echo Archives that have existed more than 30 days have been deleted >> %log_file%
echo ------------------------------------------------------------------------------------ >> %log_file%
)
)



::15

echo Checking internet connection
Ping www.google.nl -n 1 -w 100 2>&1
echo ------------------------------------------------------------------------------------ >> %log_file%
for /F "" %%i in ('time /t') do set mytime=%%i
echo %date% %mytime% >> %log_file%
if %errorlevel%==0 (
echo %compName% has internet connection >> %log_file%
) else (
echo %compName% don't have internet connection >> %log_file%
)
echo ------------------------------------------------------------------------------------ >> %log_file%



::16

if not [%5] == [] (
Ping %ip_pc% -n 1 -w 1000 2>&1
echo ------------------------------------------------------------------------------------ >> %log_file%
for /F "" %%i in ('time /t') do set mytime=%%i
echo %date% %mytime% >> %log_file%
if %errorlevel%==0 (
shutdown -s -m \\%ip_pc% -t 10 >> %log_file% 2>&1
) else (
echo ping %ip_pc% >> %log_file%
echo %compName% don't have connection to %ip_pc% >> %log_file%
)
echo ------------------------------------------------------------------------------------ >> %log_file%
)



::17

echo Getting pc list
echo ------------------------------------------------------------------------------------ >> %log_file%
for /F "" %%i in ('time /t') do set mytime=%%i
echo %date% %mytime% >> %log_file%
echo PC list:>> %log_file%
for /f "tokens=*" %%a in (pclist.txt) do (
    echo %%a >> %log_file%
)
echo ------------------------------------------------------------------------------------ >> %log_file%



::18

echo Checking if ip pc's from ipon.txt in network
echo ------------------------------------------------------------------------------------ >> %log_file%
for /F "" %%i in ('time /t') do set mytime=%%i
echo %date% %mytime% >> %log_file%
for /f "tokens=*" %%a in (ipon.txt) do (
    echo Pinging %%a >> %log_file%
    Ping %%a -n 1 -w 100 2>&1
    if errorlevel 1 (
        echo %%a not in LAN >> %log_file%
        REM email goes here
        ) else (
        echo %%a in LAN >> %log_file%
        )
)
echo ------------------------------------------------------------------------------------ >> %log_file%



::19 

echo ------------------------------------------------------------------------------------ >> %log_file%
for /F "" %%i in ('time /t') do set mytime=%%i
echo %date% %mytime% >> %log_file%

FOR /F "usebackq" %%A IN ('%log_file%') DO set size=%%~zA

if %size% LSS %size_of_log% (
    echo.File is ^< %size_of_log% bytes 2>&1
) ELSE (
    echo File %log_file% is ^>= %size_of_log% bytes >> %log_file%
)
echo ------------------------------------------------------------------------------------ >> %log_file%



::20 
echo ------------------------------------------------------------------------------------ >> %log_file%
for /F "" %%i in ('time /t') do set mytime=%%i
echo %date% %mytime% >> %log_file%
@FOR /F "skip=1 tokens=1" %%x IN ('"WMIC /node:"localhost" LOGICALDISK GET Name " ') DO (
REM @ECHO %%x
@FOR /F "tokens=1-3" %%n IN ('"WMIC /node:"localhost" LOGICALDISK GET Name,Size,FreeSpace | find /i "%%x""') DO ( @SET FreeBytes=%%n & @SET TotalBytes=%%p
SET TotalGB=0
SET FreeGB=0
:: Parameter value used to convert in GB
set num1=1074
SET /a TotalSpace=!TotalBytes:~0,-6! / !NUM1!
SET /a FreeSpace=!FreeBytes:~0,-7! / !NUM1!
SET TotalGB=!TotalSpace!22:27 21.12.2022
SET FreeGB=!FreeSpace!
SET PERNUM=100
SET /A TotalUsed=!TotalSpace! - !FreeSpace!
SET /A MULTIUSED=!TotalUsed!*!PERNUM!
SET /A PERCENTUSED=!MULTIUSED!/!TotalGB!
@echo [1 >> %log_file%
@echo Drive: %%x >> %log_file%
@echo Total space: !TotalGB! GB >> %log_file%
@echo Free space : !FreeGB! GB >> %log_file%
@echo PERCENTUSED : !PERCENTUSED! >> %log_file%
@echo 2] >> %log_file%
)
)
echo ------------------------------------------------------------------------------------ >> %log_file%



::21

for /F "" %%i in ('time /t') do set mytime=%%i
systeminfo > systeminfo_%DATE:~-10,2%.%DATE:~-7,2%.%DATE:~-4%_%mytime:~-5,2%.%mytime:~-2,2%.txt
